package dbs2013.model;

public class EffectiveChemical {
	private Integer id;
	private String name;
	private Boolean state;
	
	public EffectiveChemical(){}
	public EffectiveChemical(Integer id, String name){
		setId(id);
		setName(name);
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public Integer getId(){
		return id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setState(Boolean state){
		this.state = state;
	}
	
	public Boolean getState(){
		return state;
	}
}
