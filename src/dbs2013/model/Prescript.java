package dbs2013.model;

public class Prescript {
	private Integer id;
	private String date;
	private Integer drug_id;
	private Integer doctor_id;
	private Doctor doctor;
	private Drugs drugs;
	
	public Prescript(){}
	public Prescript(Integer id, Integer drug_id, Integer doctor_id){
		setId(id);
		setDrugId(drug_id);
		setDoctorId(doctor_id);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getDrugId() {
		return drug_id;
	}
	public void setDrugId(Integer drug_id) {
		this.drug_id = drug_id;
	}
	public Integer getDoctorId() {
		return doctor_id;
	}
	public void setDoctorId(Integer doctor_id) {
		this.doctor_id = doctor_id;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public Drugs getDrugs() {
		return drugs;
	}
	public void setDrugs(Drugs drugs) {
		this.drugs = drugs;
	}
}
