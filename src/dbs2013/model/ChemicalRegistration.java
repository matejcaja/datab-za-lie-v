package dbs2013.model;

public class ChemicalRegistration {
	private Integer id;
	private String date;
	private String valid_until;
	private Integer chemical_id;
	private EffectiveChemical chemical;
	
	public ChemicalRegistration(){}
	public ChemicalRegistration(Integer id, Integer chemical_id){
		setId(id);
		setChemicalId(chemical_id);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getValid_until() {
		return valid_until;
	}
	public void setValid_until(String valid_until) {
		this.valid_until = valid_until;
	}
	public Integer getChemicalId() {
		return chemical_id;
	}
	public void setChemicalId(Integer chemical_id) {
		this.chemical_id = chemical_id;
	}
	public EffectiveChemical getChemical() {
		return chemical;
	}
	public void setChemical(EffectiveChemical chemical) {
		this.chemical = chemical;
	}
}	
