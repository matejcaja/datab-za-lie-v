package dbs2013.model;

public class Drugs {
	private Integer id;
	private String name;
	private String holder;
	private String stav;
	private String vydaj;
	private String add;
	private String form;
	private String type_id;
	private String price;
	private String latka;
	
	public Drugs(){}
	public Drugs(Integer id){
		setId(id);
	}
	public Drugs(Integer id, String name, String holder, String stav, String vydaj,String add, String form, String type_id){
		setId(id);
		setName(name);
		setHolder(holder);
		setAdd(add);
		setStav(stav);
		setVydaj(vydaj);
		setForm(form);
		setType_id(type_id);
	}
	
	public Drugs(String name, String holder, String stav, String vydaj,String form, String price, String latka, Integer id){
		setId(id);
		setName(name);
		setHolder(holder);
		setStav(stav);
		setVydaj(vydaj);
		setForm(form);
		setPrice(price);
		setLatka(latka);
	}
	
	public String getLatka() {
		return latka;
	}
	public void setLatka(String latka) {
		this.latka = latka;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getType_id() {
		return type_id;
	}
	public void setType_id(String type_id) {
		this.type_id = type_id;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	
	public String getStav(){
		return stav;
	}
	
	public void setStav(String stav){
		this.stav = stav;
	}
	
	public String getVydaj(){
		return vydaj;
	}
	
	public void setVydaj(String vydaj){
		this.vydaj = vydaj;
	}

}
