package dbs2013.model;

public class ActualPrice {
	private Integer id;
	private String from;
	private String to;
	private Float price;
	private Drugs drugs;
	private Integer drug_id;
	

	public ActualPrice(){}
	public ActualPrice(Integer id, String from, String to,Float Price, Integer drug_id){
		setId(id);
		setFrom(from);
		setTo(to);
		setPrice(Price);
		setDrugId(drug_id);
	}
	
	public Integer getDrugId() {
		return drug_id;
	}
	public void setDrugId(Integer drug_id) {
		this.drug_id = drug_id;
	}
	
	public Drugs getDrugs() {
		return drugs;
	}
	public void setDrugs(Drugs drugs) {
		this.drugs = drugs;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
}
