package dbs2013.model;

public class Doctor {
	private Integer id;
	private String name;
	private Float salary;
	private Integer contact;
	private String pocet;
	
	public Doctor(){}
	public Doctor(Integer id, String name,Float salary, Integer contact, String pocet){
		setId(id);
		setName(name);
		setSalary(salary);
		setContact(contact);
		setPocet(pocet);
	}
	
	public String getPocet() {
		return pocet;
	}
	public void setPocet(String pocet) {
		this.pocet = pocet;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getSalary() {
		return salary;
	}
	public void setSalary(Float salary) {
		this.salary = salary;
	}
	public Integer getContact() {
		return contact;
	}
	public void setContact(Integer contact) {
		this.contact = contact;
	}
	
}
