package dbs2013.model;

public class Contents{
	private Integer id;
	private Integer drug_id;
	private Integer chemical_id;
	private Drugs drug;
	private EffectiveChemical chemical;
	private String add;
	private String name;
	private String holder;
	private String vydaj;
	private String price;
	private Integer count;

	
	public Contents(){}
	public Contents(Integer id, Integer drug_id, Integer chemical_id, String add){
		setId(id);
		setDrugId(drug_id);
		setChemicalId(chemical_id);
		setAdd(add);
	}
	public Contents(Integer drug_id, String name, String holder, String vydaj, Integer count){
		setDrugId(drug_id);
		setName(name);
		setHolder(holder);
		setVydaj(vydaj);
		//setPrice(price);
		setCount(count);
	}
	
	public Contents(Integer drug_id, String name, String holder, String vydaj, String price,Integer count){
		setDrugId(drug_id);
		setName(name);
		setHolder(holder);
		setVydaj(vydaj);
		setPrice(price);
		setCount(count);
	}
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getVydaj() {
		return vydaj;
	}
	public void setVydaj(String vydaj) {
		this.vydaj = vydaj;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	
	public Drugs getDrug() {
		return drug;
	}
	public void setDrug(Drugs drug) {
		this.drug = drug;
	}
	public EffectiveChemical getChemical() {
		return chemical;
	}
	public void setChemical(EffectiveChemical chemical) {
		this.chemical = chemical;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDrugId() {
		return drug_id;
	}
	public void setDrugId(Integer drugId) {
		this.drug_id = drugId;
	}
	public Integer getChemicalId() {
		return chemical_id;
	}
	public void setChemicalId(Integer chemicalId) {
		this.chemical_id = chemicalId;
	}
}
