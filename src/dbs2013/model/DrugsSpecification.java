package dbs2013.model;

public class DrugsSpecification {
	private Integer id;
	private String date;
	private Integer drug_id;
	private Integer type_id;
	private DrugsForm form;
	private Drugs drugs;
	
	public DrugsSpecification(){}
	public DrugsSpecification(Integer id, Integer drug_id, Integer type_id){
		setId(id);
		setDrugId(drug_id);
		setTypeId(type_id);
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getDrugId() {
		return drug_id;
	}
	public void setDrugId(Integer drug_id) {
		this.drug_id = drug_id;
	}
	public Integer getTypeId() {
		return type_id;
	}
	public void setTypeId(Integer type_id) {
		this.type_id = type_id;
	}
	public DrugsForm getForm() {
		return form;
	}
	public void setForm(DrugsForm form) {
		this.form = form;
	}
	public Drugs getDrugs() {
		return drugs;
	}
	public void setDrugs(Drugs drugs) {
		this.drugs = drugs;
	}
}
