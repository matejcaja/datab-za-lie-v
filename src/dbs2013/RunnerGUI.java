package dbs2013;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Choice;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;

import dbs2013.manager.ContentsManager;
import dbs2013.manager.RunnerManager;
import dbs2013.manager.DrugsManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;


import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JSeparator;


public class RunnerGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JDialog dialog = new JDialog();
	private JDialog dialog2 = new JDialog();
	private JDialog dialog3 = new JDialog();
	private JPanel contentPane;
	private CardLayout cardLayout = new CardLayout(0, 0);
	private JPanel card1 = new JPanel();
	private JPanel card2 = new JPanel();
	private JPanel card3 = new JPanel();
	private JPanel card4 = new JPanel();
	private JPanel card5 = new JPanel();
	private JPanel card6 = new JPanel();
	private JPanel card7 = new JPanel();
	private JPanel card8 = new JPanel();
	final JTextArea textArea_1 = new JTextArea();
	final DefaultListModel listModel = new DefaultListModel();
	final JList list = new JList(listModel);
	final DefaultListModel listModel2 = new DefaultListModel();
	final DefaultListModel listModel3 = new DefaultListModel();
	final JList list3 = new JList(listModel3);
	final JList list2 = new JList(listModel2);
	private final JButton btnLieky = new JButton("Zoznam Liekov");
	private final JButton btnShowAll = new JButton("show All");
	JButton btnFormatovanie = new JButton("Zaznam Lieku");
	private JTextArea textArea = new JTextArea();
	ArrayList<ArrayList<String>> Array = new ArrayList<ArrayList<String>>();
	ArrayList<String> col; 
	String data;

	/**
	 * Launch the application.
	 */
	DrugsManager dr = new DrugsManager();
	ContentsManager co = new ContentsManager();
	RunnerManager run = new RunnerManager();
	private final JButton btnNewButton = new JButton("advanced");
	private final JLabel lblNewLabel = new JLabel("Vymazanie zaznamu:");
	private final JButton btnDelete = new JButton("delete");
	private final JScrollPane scrollPane_4 = new JScrollPane();
	private final JButton btnNewButton_2 = new JButton("OK");
	private final JButton btnAdd = new JButton("add");
	private final JButton btnBack_2 = new JButton("back");
	private final JLabel lblNewLabel_1 = new JLabel("Nazov Lieku:");
	private JTextField textField;
	private JTextField textField_1;
	private final JScrollPane scrollPane_6 = new JScrollPane();
	final DefaultListModel listModel4 = new DefaultListModel();
	final DefaultListModel listModel5 = new DefaultListModel();
	final DefaultListModel listModel6 = new DefaultListModel();
	private final JList list4 = new JList(listModel4);
	private final JScrollPane scrollPane_7 = new JScrollPane();
	private final JScrollPane scrollPane_8 = new JScrollPane();
	private final JList list5 = new JList(listModel5);
	private final JList list6 = new JList(listModel6);
	private final JScrollPane scrollPane_5 = new JScrollPane();
	final DefaultListModel listModel7 = new DefaultListModel();
	final DefaultListModel listModel8 = new DefaultListModel();
	private final JList list7 = new JList(listModel7);
	private final JLabel lblLiecivaLatka = new JLabel("Lieciva latka");
	private final JScrollPane scrollPane_9 = new JScrollPane();
	private final JList list8 = new JList(listModel8);
	private final JTextArea textArea_2 = new JTextArea();
	private final JButton btnBack_3 = new JButton("back");
	private final JButton btnAdd_1 = new JButton("add");
	private final JButton btnUpdate = new JButton("update");
	private final JButton btnNewButton_3 = new JButton("back");
	private final JScrollPane scrollPane_10 = new JScrollPane();
	final DefaultListModel listModel9 = new DefaultListModel();
	final DefaultListModel listModel10 = new DefaultListModel();
	private final JList list9 = new JList(listModel9);
	private final JLabel lblZvolteLiekKtory = new JLabel("Zvolte liek:");
	private final JLabel lblZvolteHolderaLieku = new JLabel("Zvolte holdera lieku:");
	private final JScrollPane scrollPane_11 = new JScrollPane();
	private final JList list10 = new JList(listModel10);
	private final JTextField textField_2 = new JTextField();
	private final JLabel lblZadajteNovuCenu = new JLabel("Zadajte novu cenu lieku:");
	private final JButton btnOk = new JButton("ok");
	private final JScrollPane scrollPane_12 = new JScrollPane();
	private final JTextArea textArea_3 = new JTextArea();
	private final JButton btnShow = new JButton("show");
	private final JScrollPane scrollPane_13 = new JScrollPane();
	private final JTextArea textArea_4 = new JTextArea();
	private final JLabel lblNewLabel_3 = new JLabel("Databazove systemy\n");
	private final JLabel lblMysqlJavaProgram = new JLabel("MySQL java program (c) Matej Caja ");
	private final JLabel label = new JLabel("2012/2013");
	private final JLabel lblNewLabel_4 = new JLabel("Zadajte novu dostupnost: ");
	private final JSeparator separator_1 = new JSeparator();
	private final JScrollPane scrollPane_14 = new JScrollPane();
	final DefaultListModel listModel11 = new DefaultListModel();
	private final JList list11 = new JList(listModel11);
	private final JButton btnZaznamLekarov = new JButton("Zaznam lekarov");
	private final JButton btnBack_4 = new JButton("back");
	private final JScrollPane scrollPane_15 = new JScrollPane();
	private final JTextArea textArea_5 = new JTextArea();
	private final JButton btnShowAll_1 = new JButton("show all");
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RunnerGUI frame = new RunnerGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public void showSpecificDrugDialog(){
		final JButton btnNewButton = new JButton("OK");
		final DefaultListModel listModel11 = new DefaultListModel();
		final JList list11 = new JList(listModel11);
		final Choice myChoice2 = new Choice();
		final JScrollPane scrollPaneD = new JScrollPane();
		final JLabel label1 = new JLabel();
		final JLabel label2 = new JLabel();
		
		String name[] = {"PharmaGuy", "DrugStore"};
		btnNewButton.setBounds(270, 200, 80, 25);
		label1.setBounds(20,10,200,20);
		label2.setBounds(20,40,200,20);
		
		
		myChoice2.setBounds(250, 10, 100, 20);
		
		label1.setText("Zadajte meno holdera:");
		label2.setText("Zadajte meno lieku:");
		
		
		for (int i = 0; i < 2; i++){
            myChoice2.add(name[i]);
		}
		
		run.getListofDrugsToShow(textArea, contentPane);
		run.setListName(listModel11);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog3.dispose();
				run.showAdvancedDrug(textArea_4, list11, ("\""+myChoice2.getSelectedItem()+"\""), contentPane);
				run.runnerList.clear();
				run.clearAll();
				clearAllModels(); 
			}
		});
			
		dialog3.setVisible(true);
		scrollPaneD.setViewportView(list11);
		scrollPaneD.setBounds(250, 50, 120, 120);
		dialog3.setSize(400,250);
        dialog3.setLocationRelativeTo(contentPane);
        dialog3.getContentPane().setLayout(null);
        dialog3.getContentPane().add(btnNewButton);
        dialog3.getContentPane().add(label1);
        dialog3.getContentPane().add(label2);
        
        dialog3.getContentPane().add(myChoice2);
        dialog3.getContentPane().add(scrollPaneD);
 
       
        
	}
	
	public void showConfirmDialog(){
		final JButton btnNewButton = new JButton("OK");
		final JButton btnNewButton2 = new JButton("Run!");
		final JLabel label1 = new JLabel("Ste si isty operaciou?");
		btnNewButton.setBounds(165, 80, 60, 25);
		btnNewButton2.setBounds(65, 80, 70, 25);
		label1.setBounds(70, 50, 200, 25);
		
		
		btnNewButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText("");
				dialog2.dispose();
				
			}
		});
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.drugDeletedFinally(textArea, list, list2, contentPane);
				textArea_1.setText("");
				textArea_1.setText("done!");
				dialog2.dispose();
			}
		});
		
		dialog2.setVisible(true);
		dialog2.setSize(300,150);
        dialog2.setLocationRelativeTo(contentPane);
        dialog2.getContentPane().setLayout(null);
        dialog2.getContentPane().add(label1);
        dialog2.getContentPane().add(btnNewButton);
        dialog2.getContentPane().add(btnNewButton2);
	}

	public void showDialog(){
		
		final JButton btnNewButton = new JButton("OK");
		final JTextField textField2 = new JTextField("1000");
		final JTextField textField3 = new JTextField("1");
		final Choice myChoice = new Choice();
		final Choice myChoice2 = new Choice();
		final JRadioButton radio = new JRadioButton("Ano");
		final JLabel label1 = new JLabel();
		final JLabel label2 = new JLabel();
		final JLabel label3 = new JLabel();
		final JLabel label4 = new JLabel();
		final JLabel label5 = new JLabel("Pocet liecivych latok:");
		String state[] = {"asc","desc"};
		String name[] = {"PharmaGuy", "DrugStore"};
		btnNewButton.setBounds(270, 200, 80, 25);
		textField2.setBounds(250,100,100,20);
		textField3.setBounds(250,130,100,20);
		label1.setBounds(20,10,200,20);
		label2.setBounds(20,40,200,20);
		label3.setBounds(20,70,210,20);
		label4.setBounds(20,100,200,20);
		label5.setBounds(20, 130, 200, 20);
		myChoice.setBounds(250, 40, 100, 20);
		myChoice2.setBounds(250, 10, 100, 20);
		radio.setBounds(250,70,100,20);
		label1.setText("*Zadajte meno holdera:");
		label2.setText("Zadajte sposob zoradenia:");
		label3.setText("Zobrazit iba dostupne lieky:");
		label4.setText("Cenovy limit: ");
		data = null;
		
		for (int i = 0; i < 2; i++){
            myChoice.add(state[i]);
            myChoice2.add(name[i]);
		}
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.setData(myChoice2.getSelectedItem());
				run.setOrder(myChoice.getSelectedItem());
				
				if(radio.isSelected() == true){
					run.setAble(radio.getText());
				}else{run.setAble("null");}
				
				run.setPrice(textField2.getText());
				run.setCount(textField3.getText());
				dialog.dispose();	
				textArea.setText("");
				run.getAdvancedInfoDrugs(textArea, contentPane);
			}
		});
			
		dialog.setVisible(true);

		dialog.setSize(400,250);
        dialog.setLocationRelativeTo(contentPane);
        dialog.getContentPane().setLayout(null);
        dialog.getContentPane().add(btnNewButton);
        dialog.getContentPane().add(textField2);
        dialog.getContentPane().add(textField3);
        dialog.getContentPane().add(label1);
        dialog.getContentPane().add(label2);
        dialog.getContentPane().add(myChoice);
        dialog.getContentPane().add(myChoice2);
        dialog.getContentPane().add(radio);
        dialog.getContentPane().add(label3);
        dialog.getContentPane().add(label4);
        dialog.getContentPane().add(label5);
        
        
	}
	
	
	public RunnerGUI() {
		textField_2.setBounds(78, 193, 114, 19);
		textField_2.setColumns(10);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 578, 478);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(cardLayout);
		contentPane.add(card1, "1");
		contentPane.add(card2, "2");
		contentPane.add(card3, "3");
		contentPane.add(card4, "4");
		contentPane.add(card5, "5");
		contentPane.add(card6, "6");
		contentPane.add(card7, "7");
		card7.setLayout(null);
		
		btnBack_4.setBounds(420, 384, 117, 25);
		
		card7.add(btnBack_4);
		scrollPane_15.setBounds(52, 156, 486, 203);
		
		card7.add(scrollPane_15);
		
		scrollPane_15.setViewportView(textArea_5);
		
		btnShowAll_1.setBounds(52, 53, 117, 25);
		
		card7.add(btnShowAll_1);
		card6.setLayout(null);
		
		
		btnNewButton_3.setBounds(422, 385, 117, 25);
		
		card6.add(btnNewButton_3);
		scrollPane_10.setBounds(78, 50, 126, 79);
		
		card6.add(scrollPane_10);
		
		scrollPane_10.setViewportView(list9);
		lblZvolteLiekKtory.setBounds(78, 23, 145, 15);
		
		card6.add(lblZvolteLiekKtory);
		lblZvolteHolderaLieku.setBounds(310, 23, 197, 15);
		
		card6.add(lblZvolteHolderaLieku);
		scrollPane_11.setBounds(310, 50, 126, 45);
		
		card6.add(scrollPane_11);
		
		scrollPane_11.setViewportView(list10);
		
		card6.add(textField_2);
		lblZadajteNovuCenu.setBounds(78, 169, 237, 15);
		
		card6.add(lblZadajteNovuCenu);
		
		btnOk.setBounds(78, 385, 117, 25);
		
		card6.add(btnOk);
		scrollPane_12.setBounds(78, 295, 417, 63);
		
		card6.add(scrollPane_12);
		
		scrollPane_12.setViewportView(textArea_3);
		lblNewLabel_4.setBounds(299, 169, 197, 15);
		
		card6.add(lblNewLabel_4);
		separator_1.setBounds(78, 141, 417, 2);
		
		card6.add(separator_1);
		scrollPane_14.setBounds(299, 193, 117, 51);
		
		card6.add(scrollPane_14);
		
		scrollPane_14.setViewportView(list11);
		card5.setLayout(null);
		btnBack_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearAllArea();
				run.clearAll();
				clearAllModels();
				cardLayout.show(contentPane, "1");
			}
		});
		btnBack_3.setBounds(409, 400, 117, 25);
		
		card5.add(btnBack_3);
		btnAdd_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.getListofDrugsToShow(textArea, contentPane);
				run.getListOfEffectiveChemical();
				run.getForm();
				run.setListForm(listModel4);
				run.setListHolder(listModel5);
				run.setListStav(listModel6);
				run.setListVydaj(listModel7);
				run.setListEffectiveChemical(listModel8);
				cardLayout.show(contentPane, "4");
				
			}
		});
		btnAdd_1.setBounds(40, 44, 117, 25);
		
		card5.add(btnAdd_1);
		
		btnUpdate.setBounds(40, 81, 117, 25);
		
		card5.add(btnUpdate);
		
		btnShow.setBounds(40, 118, 117, 25);
		
		card5.add(btnShow);
		scrollPane_13.setBounds(40, 161, 489, 196);
		
		card5.add(scrollPane_13);
		
		scrollPane_13.setViewportView(textArea_4);
		card4.setLayout(null);
		
		btnNewButton_2.setBounds(281, 394, 117, 25);
		
		card4.add(btnNewButton_2);
		btnBack_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearAllArea();
				run.clearAll();
				clearAllModels();
				cardLayout.show(contentPane, "1");
			}
		});
		btnBack_2.setBounds(410, 394, 117, 25);
		
		card4.add(btnBack_2);
		lblNewLabel_1.setBounds(58, 0, 134, 15);
		
		card4.add(lblNewLabel_1);
		
		textField = new JTextField("Liek");
		textField.setBounds(58, 27, 114, 19);
		card4.add(textField);
		textField.setColumns(10);
		
		JLabel lblMenoHoldera = new JLabel("Meno holdera:");
		lblMenoHoldera.setBounds(58, 56, 134, 15);
		card4.add(lblMenoHoldera);
		
		JLabel lblStavLieku = new JLabel("Stav Lieku:");
		lblStavLieku.setBounds(278, 58, 117, 15);
		card4.add(lblStavLieku);
		
		JLabel lblNewLabel_2 = new JLabel("Vydaj: ");
		lblNewLabel_2.setBounds(60, 285, 70, 15);
		card4.add(lblNewLabel_2);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(58, 150, 453, 2);
		card4.add(separator);
		
		JLabel lblCenaLieku = new JLabel("Cena lieku:");
		lblCenaLieku.setBounds(278, 315, 96, 15);
		card4.add(lblCenaLieku);
		
		textField_1 = new JTextField("1000");
		textField_1.setBounds(281, 342, 114, 19);
		card4.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblFormaLieku = new JLabel("Forma Lieku: ");
		lblFormaLieku.setBounds(58, 164, 96, 15);
		card4.add(lblFormaLieku);
		scrollPane_6.setBounds(58, 191, 117, 83);
		
		card4.add(scrollPane_6);
		list11.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list11.setLayoutOrientation(JList.VERTICAL);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		scrollPane_6.setViewportView(list4);
		scrollPane_7.setBounds(58, 83, 103, 53);
		
		card4.add(scrollPane_7);
		
		scrollPane_7.setViewportView(list5);
		scrollPane_8.setBounds(278, 85, 108, 53);
		
		card4.add(scrollPane_8);
		
		scrollPane_8.setViewportView(list6);
		card2.setLayout(null);
		btnShowAll.setBounds(12, 32, 117, 25);
		card2.add(btnShowAll);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 120, 542, 261);
		card2.add(scrollPane);
		scrollPane.setViewportView(textArea);
		btnNewButton.setBounds(12, 69, 117, 25);
		card2.add(btnNewButton);
		
		JLabel lblZobrazenieZoznamu = new JLabel("Vypisy:");
		lblZobrazenieZoznamu.setBounds(12, 5, 162, 15);
		card2.add(lblZobrazenieZoznamu);
		
		JButton btnBack = new JButton("back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearAllArea();
				run.clearAll();
				clearAllModels();
				textArea.setText("");
				cardLayout.show(contentPane, "1");
			}
		});
		btnBack.setBounds(487, 402, 67, 25);
		card2.add(btnBack);
		lblNewLabel.setBounds(171, 5, 162, 15);
		
		card2.add(lblNewLabel);
		
		btnDelete.setBounds(171, 32, 117, 25);
		
		card2.add(btnDelete);
		
		btnAdd.setBounds(364, 32, 117, 25);
		
		card2.add(btnAdd);
		
		JLabel lblPridanieZaznamu = new JLabel("Pridanie zaznamu:");
		lblPridanieZaznamu.setBounds(364, 5, 162, 15);
		card2.add(lblPridanieZaznamu);
		
		card1.setLayout(null);
		btnLieky.setBounds(207, 209, 150, 42);
		card1.add(btnLieky);
		btnFormatovanie.setBounds(207, 265, 150, 42);
		card1.add(btnFormatovanie);
		lblNewLabel_3.setBounds(211, 100, 168, 15);
		
		card1.add(lblNewLabel_3);
		lblMysqlJavaProgram.setBounds(162, 127, 265, 15);
		
		card1.add(lblMysqlJavaProgram);
		label.setBounds(246, 154, 70, 15);
		
		card1.add(label);
		
		btnZaznamLekarov.setBounds(207, 319, 150, 42);
		
		card1.add(btnZaznamLekarov);
		card3.setLayout(null);
		
		JLabel lblZadajteMenoLieku = new JLabel("Zadajte meno lieku:");
		lblZadajteMenoLieku.setBounds(24, 12, 165, 15);
		card3.add(lblZadajteMenoLieku);
		
		JLabel lblZadajteHolderaLieku = new JLabel("Zadajte holdera lieku:");
		lblZadajteHolderaLieku.setBounds(217, 12, 165, 15);
		card3.add(lblZadajteHolderaLieku);
		
		JLabel lblZadajteForm = new JLabel("Vyberte formu lieku:");
		lblZadajteForm.setBounds(217, 95, 168, 15);
		card3.add(lblZadajteForm);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.getListofDrugsToShow(textArea, contentPane);
				run.getListOfEffectiveChemical();
				run.getForm();
				run.setListForm(listModel4);
				run.setListHolder(listModel5);
				run.setListStav(listModel6);
				run.setListVydaj(listModel7);
				run.setListEffectiveChemical(listModel8);
				cardLayout.show(contentPane, "4");
			}
		});
		
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.insertNewDrug(textArea_2,textField, textField_1,list4, list5, list6, list7, list8,contentPane);
				run.clearAll();
				clearAllModels();
			}
		});
		
		
		final JButton btnNewButton_1 = new JButton("Vymazat");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showConfirmDialog();
				btnNewButton_1.setEnabled(false);
			}
		});
		btnNewButton_1.setEnabled(false);
		btnNewButton_1.setBounds(217, 372, 117, 25);
		card3.add(btnNewButton_1);
		
		JButton btnBack_1 = new JButton("back");
		btnBack_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearAllArea();
				cardLayout.show(contentPane, "2");
				run.runnerList.clear();
				run.runnerList2.clear();
				run.runnerList3.clear();
				listModel2.clear();
				listModel.clear();
				listModel3.clear();
				textArea_1.setText("");
			}
		});
		btnBack_1.setBounds(489, 403, 67, 25);
		card3.add(btnBack_1);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(24, 39, 114, 172);
		card3.add(scrollPane_1);
		
		
		
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		
		scrollPane_1.setViewportView(list);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(217, 39, 114, 44);
		card3.add(scrollPane_2);
		
		
		list2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list2.setLayoutOrientation(JList.VERTICAL);
		scrollPane_2.setViewportView(list2);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(217, 122, 114, 89);
		card3.add(scrollPane_3);
		
		
		list3.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list3.setLayoutOrientation(JList.VERTICAL);
		scrollPane_3.setViewportView(list3);
		list4.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list4.setLayoutOrientation(JList.VERTICAL);
		list5.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list5.setLayoutOrientation(JList.VERTICAL);
		list6.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list6.setLayoutOrientation(JList.VERTICAL);
		list7.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list7.setLayoutOrientation(JList.VERTICAL);
		list8.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list8.setLayoutOrientation(JList.VERTICAL);
		scrollPane_5.setBounds(58, 313, 96, 57);
		
		card4.add(scrollPane_5);
		
		scrollPane_5.setViewportView(list7);
		lblLiecivaLatka.setBounds(278, 164, 122, 15);
		
		card4.add(lblLiecivaLatka);
		scrollPane_9.setBounds(273, 191, 122, 112);
		
		card4.add(scrollPane_9);
		
		scrollPane_9.setViewportView(list8);
		textArea_2.setBounds(58, 382, 201, 36);
		
		card4.add(textArea_2);
		JButton btnNahlad = new JButton("Nahlad");
		btnNahlad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				run.showDrugToDelete(textArea_1, list, list2, list3, contentPane);
				btnNewButton_1.setEnabled(true);
			}
		});
		btnNahlad.setBounds(419, 229, 117, 25);
		card3.add(btnNahlad);
		scrollPane_4.setBounds(24, 277, 512, 60);
		
		card3.add(scrollPane_4);
		scrollPane_4.setViewportView(textArea_1);
		
		btnLieky.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				run.clearAll();
				clearAllModels();
				cardLayout.show(contentPane, "2");
			}
		});
		
		btnFormatovanie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.clearAll();
				clearAllModels();
				cardLayout.show(contentPane, "5");
			}
		});
		
		btnShowAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					textArea.setText("");
					run.getAllInfoDrugs(textArea,contentPane);
			}
		});
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showDialog();
			}
		});
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.getListofDrugsToDelete(textArea, contentPane);
				run.setListName(listModel);
				run.setListHolder(listModel2);
				run.setListForm(listModel3);			
				cardLayout.show(contentPane, "3");
			}
		});
		
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.clearAll();
				clearAllModels();
				run.setListStav(listModel11);
				run.getListofDrugsToShow(textArea, contentPane);
				run.setListHolder(listModel10);
				run.setListName(listModel9);
				cardLayout.show(contentPane, "6");
			}
		});
		
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_4.setText("");
				run.clearAll();
				clearAllModels();
				cardLayout.show(contentPane, "5");
			}
		});
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.getUpdate(textArea_3,list9, list10, list11,textField_2);
			}
		});
		
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showSpecificDrugDialog();
			}
		});
		btnZaznamLekarov.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPane, "7");
			}
		});
		
		btnBack_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPane, "1");
			}
		});
		
		btnShowAll_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				run.infoDoctor(textArea_5);
			}
		});
	}
	
	
	
	public void clearAllModels(){
		listModel.clear();
		listModel2.clear();
		listModel3.clear();
		listModel4.clear();
		listModel5.clear();
		listModel6.clear();
		listModel7.clear();
		listModel8.clear();
		listModel9.clear();
		listModel9.clear();
		listModel10.clear();
		listModel11.clear();
	}
	
	public void clearAllArea(){
		textArea_1.setText("");
		textArea_2.setText("");
		textArea_3.setText("");
		textArea_4.setText("");
	}
}
