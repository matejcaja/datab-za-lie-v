package dbs2013.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JOptionPane;

public abstract class DatabaseConnect {
		
		@SuppressWarnings({ "finally", "rawtypes" })
		protected ArrayList selectQuery(String queryString) throws SQLException{

		ArrayList<Object> result = new ArrayList<Object>();
		Connection conn = null;
		Statement stmt = null;
		Properties connectionProps = new Properties();
	    connectionProps.put("user", "root");
	    connectionProps.put("password", "work2012");
	    String connectionString = "jdbc:mysql://localhost:3306/DBS2013?allowMultiQueries=true";
	    
	    try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	    
	    try {
			conn = DriverManager.getConnection(connectionString, connectionProps);
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(queryString);
				
			while(rs.next()){
				result.add(processRow(rs));
			}
	
			
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error handling the database!", "Database Error!" , JOptionPane.ERROR_MESSAGE);
			
		} finally {
			stmt.close();
			return result;
		}
	}
		
		@SuppressWarnings({ "finally", "rawtypes" })
		protected ArrayList updateQuery(String queryString) throws SQLException{

		ArrayList<Object> result = new ArrayList<Object>();
		Connection conn = null;
		Statement stmt = null;
		Properties connectionProps = new Properties();
	    connectionProps.put("user", "root");
	    connectionProps.put("password", "work2012");
	    String connectionString = "jdbc:mysql://localhost:3306/DBS2013?allowMultiQueries=true";
	    
	    try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	    
	    try {
			conn = DriverManager.getConnection(connectionString, connectionProps);
			stmt = conn.createStatement();
			stmt.executeUpdate(queryString);
			
					
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error handling update into database!", "Database Error!" , JOptionPane.ERROR_MESSAGE);
			
		} finally {
			stmt.close();
			return result;
		}
	}
      

	protected abstract Object processRow(ResultSet rs) throws SQLException;

}
