package dbs2013.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.model.Drugs;
import dbs2013.manager.DatabaseConnect;

public class DrugsManager extends DatabaseConnect{
	
	protected Drugs processRow(ResultSet rs) throws SQLException{
		return(new Drugs(rs.getInt("id"), rs.getString("name"), rs.getString("holder"), rs.getString("price"), 
				rs.getString("stav"), rs.getString("vydaj"), rs.getString("type"), rs.getString("drug_id")));
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> getAllDrugs() throws SQLException{
		return selectQuery("Select *, a.price from Drugs d join ActualPrice a on a.drug_id = d.id");
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> listDrugToDelete() throws SQLException{
		return selectQuery("select * from Drugs d "+
				"join DrugsSpecification s on s.drug_id = d.id "+
				"join DrugsForm f on f.id = s.type_id "+
				"join ActualPrice a on a.drug_id = d.id "+
				"group by d.name");
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> drugToDelete(String name, String holder, String form) throws SQLException{
		return selectQuery("select *,s.type_id from Drugs d "+
				"join DrugsSpecification s on s.drug_id = d.id "+
				"join DrugsForm f on f.id = s.type_id "+
				"join ActualPrice a on a.drug_id = d.id "+
				"where d.name like "+ name+ " and d.holder like " +holder+" and f.`type` like " +form);
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> drugToList() throws SQLException{
		return selectQuery("select *,s.type_id from Drugs d "+
				"join DrugsSpecification s on s.drug_id = d.id "+
				"join DrugsForm f on f.id = s.type_id "+
				"join ActualPrice a on a.drug_id = d.id " +
				"group by d.name");
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> drugDeleted(String name, String holder) throws SQLException{
		return updateQuery("delete from Drugs where name = "+name+" and holder = "+holder);
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> listForms() throws SQLException{
		return selectQuery("select * from Drugs d "+
				"join DrugsSpecification s on s.drug_id = d.id "+
				"join DrugsForm f on f.id = s.type_id "+
				"join ActualPrice a on a.drug_id = d.id "+
				"group by f.`type` order by f.id");
	}
	
	
	/*********************update drug**************************/
	
	@SuppressWarnings("unchecked")
	public List<Drugs> findDrugToUpdate(String name, String holder) throws SQLException{
		return selectQuery("select *,s.type_id from Drugs d "+
				"join DrugsSpecification s on s.drug_id = d.id "+
				"join DrugsForm f on f.id = s.type_id "+
				"join ActualPrice a on a.drug_id = d.id "+
				"where d.name like "+ name+ " and d.holder like " +holder+" group by d.id");
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> oldPriceUpdate(Integer drug_id) throws SQLException{
		return updateQuery("update ActualPrice a set a.to = curdate() where drug_id = "+drug_id +" and a.to is null");
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> createNewPrice(Integer drug_id, String price) throws SQLException{
		return updateQuery("insert into ActualPrice (`from`,`price`,`drug_id`) values " +
				"(curdate(),"+price+","+drug_id+")");
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> createNewStav(Integer drug_id, String stav) throws SQLException{
		return updateQuery("update Drugs d set d.stav = "+stav+" where d.id = "+drug_id);
		
	}
	
	/*********************Advanced Drug****************************/
	
	
}
