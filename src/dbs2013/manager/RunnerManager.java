package dbs2013.manager;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import dbs2013.model.ActualPrice;
import dbs2013.model.Contents;
import dbs2013.model.Doctor;
import dbs2013.model.Drugs;
import dbs2013.model.EffectiveChemical;

public class RunnerManager {
	
	DrugsManager dr = new DrugsManager();
	ContentsManager co = new ContentsManager();
	EffectiveChemicalManager ech = new EffectiveChemicalManager();
	AdvancedDrugManager adr = new AdvancedDrugManager();
	NewDrugsManager ndr = new NewDrugsManager();
	DoctorManager doc = new DoctorManager();
	PriceManager pr = new PriceManager();
	String data;
	String order = null;
	String able = null;
	String price = "1000";
	String count = "1";
	public ArrayList<String> runnerList = new ArrayList<String>();
	public ArrayList<String> runnerList2 = new ArrayList<String>();
	public ArrayList<String> runnerList3 = new ArrayList<String>();
	public ArrayList<String> runnerList4 = new ArrayList<String>();
	public ArrayList<String> runnerList5 = new ArrayList<String>();
	public ArrayList<String> runnerList6 = new ArrayList<String>();
	public ArrayList<String> runnerList7 = new ArrayList<String>();
	
	public void getAllInfoDrugs(JTextArea textArea, JPanel contentPane){
		textArea.append("ID\tNazov Lieku\tVlastnik\tVydaj\tCena\tLiecive Latky\n" +
				"------------------------------------------------------------------" +
				"------------------------------------------------------------------\n");
		
		try {
			for(Contents contents : co.getAllInfo()){
				textArea.append(contents.getDrugId() +"\t"+ contents.getName() + "\t" + contents.getHolder() + "\t"+
						contents.getVydaj()+"\t"+ contents.getPrice() +"\t"+ contents.getCount() +"\n");
			}
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(contentPane, "Error","File error!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void getAdvancedInfoDrugs(JTextArea textArea, JPanel contentPane){
		textArea.append("ID\tNazov Lieku\tVlastnik\tVydaj\tCena\tLiecive Latky\n" +
				"------------------------------------------------------------------" +
				"------------------------------------------------------------------\n");
		
		try {
			for(Contents contents : co.getAdvancedInfo(data, order, able, price, count)){
				textArea.append(contents.getDrugId() +"\t"+ contents.getName() + "\t" + contents.getHolder() + "\t"+
						contents.getVydaj()+"\t"+ contents.getPrice() +"\t"+ contents.getCount() +"\n");
			}
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(contentPane, "Error","File error!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void getForm(){
		try {
			for(Drugs drugs : dr.listForms()){
				runnerList3.add(drugs.getForm());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void getListofDrugsToDelete(JTextArea textArea, JPanel contentPane){
		try {
			for(Drugs drugs : dr.listDrugToDelete()){
				runnerList.add(drugs.getName());
			}
			for(Drugs drugs : dr.listForms()){
				runnerList3.add(drugs.getForm());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void getListOfEffectiveChemical(){
		try {
			for(EffectiveChemical chemical : ech.listOfEffectiveChemical()){
				runnerList6.add(chemical.getName());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void getListofDrugsToShow(JTextArea textArea, JPanel contentPane){
		try {
			for(Drugs drugs : dr.drugToList()){
				runnerList.add(drugs.getName());
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void showDrugToDelete(JTextArea textArea, JList list1, JList list2, JList list3, JPanel contentPane){
		String name = "'"+(runnerList.get(list1.getSelectedIndex())+"'");
		String holder = "'"+(runnerList2.get(list2.getSelectedIndex())+"'");
		String form = "'"+(runnerList3.get(list3.getSelectedIndex())+"'");
		
		try {
			for(Drugs drugs : dr.drugToDelete(name, holder, form)){
				textArea.append("Liek: "+drugs.getName() + "\n" +"Holder: "+ drugs.getHolder() +"\n"+ "Forma lieku: "+drugs.getForm()+"\n");
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(contentPane, "Error","File error!", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public void drugDeletedFinally(JTextArea textArea, JList list1, JList list2, JPanel contentPane){
		String name = "\""+(runnerList.get(list1.getSelectedIndex())+"\"");
		String holder = "\""+(runnerList2.get(list2.getSelectedIndex())+"\"");
		try {
			dr.drugDeleted(name, holder);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(contentPane, "Error","File error!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	public Integer getNewDrugId(String name){
		try {
			for(Drugs drug : ndr.listIdofNewDrug(name)){
				return drug.getId();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
		
	}
	
	public void insertNewDrug(JTextArea textArea,JTextField a, JTextField b, JList list1, JList list2,  JList list3, JList list4, JList list5,JPanel contentPane){
		String name = "\""+a.getText()+"\"";
		String price = b.getText();
		String holder = "\""+(runnerList2.get(list2.getSelectedIndex())+"\"");
		String stav = "\""+(runnerList5.get(list4.getSelectedIndex())+"\""); 
		String vydaj = "\""+(runnerList4.get(list3.getSelectedIndex())+"\"");		
		
		try {
			ndr.insertIntoDrugs(name, holder, stav, vydaj);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int i = getNewDrugId(name);
		
		try {
			ndr.insertIntoPrice(i, price);
			ndr.insertIntoForm(i, (list1.getSelectedIndex()+1));
			ndr.insertIntoContents(i, (list5.getSelectedIndex()+1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		textArea.setText("done!");
		
	}
	
	
	public void setListEffectiveChemical(DefaultListModel list){
		for(int i = 0; i < runnerList6.size(); i++){
			list.addElement(runnerList6.get(i));
		}
	}
	
	public void setListName(DefaultListModel list){
		for(int i = 0; i < runnerList.size(); i++){
			list.addElement(runnerList.get(i));
		}
	}
	
	public void setListHolder(DefaultListModel list){
		runnerList2.add("DrugStore");
		runnerList2.add("PharmaGuy");
		
		for(int i = 0; i < runnerList2.size(); i++){
			list.addElement(runnerList2.get(i));
		}

	}
	
	public void setListStav(DefaultListModel list){
		runnerList5.add("able");
		runnerList5.add("not able");
		
		for(int i = 0; i < runnerList5.size(); i++){
			list.addElement(runnerList5.get(i));
		}
	}
	
	public void setListVydaj(DefaultListModel list){
		runnerList4.add("free");
		runnerList4.add("on prescript");
		
		for(int i = 0; i < runnerList4.size(); i++){
			list.addElement(runnerList4.get(i));
		}
	}
	
	public void setListForm(DefaultListModel list){
		
		for(int i = 0; i < runnerList3.size(); i++){
			list.addElement(runnerList3.get(i));
		}
	}
	
	public void setData(String data){
		String a = "'"+data+"'";
		this.data = a;
	}
	
	public void setOrder(String order){
		this.order = order;
	}
	
	public void setAble(String able){
		if(able.equals("Ano"))
			this.able = "'able'";
		else
			this.able = "'not able'";
		
	}
	
	public void setPrice(String price){
		this.price = price;
	}
	
	public void setCount(String count){
		this.count = count;
	}
	
	/**************************UPDATE**************************/
	
	public void getUpdate(JTextArea textArea, JList list1, JList list2,JList list3, JTextField a){
		int i = 0;
		String name = "\""+(runnerList.get(list1.getSelectedIndex())+"\"");
		String holder = "\""+(runnerList2.get(list2.getSelectedIndex())+"\"");
		
		textArea.append("Liek, ktory sa updateuje: "+runnerList.get(list1.getSelectedIndex())+"\n"+
				"Holder, ktoremu liek patri: "+runnerList2.get(list2.getSelectedIndex()) + "\n"+
				"Nova cena: " +a.getText()+"\n");
	
		try {
			for(Drugs drug : dr.findDrugToUpdate(name, holder)){
				 i = drug.getId();	
			}
			dr.oldPriceUpdate(i);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			dr.createNewPrice(i, a.getText());
			dr.createNewStav(i, ("\""+runnerList5.get(list3.getSelectedIndex())+"\""));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		textArea.append("done!\n");
	}
	/*********************Advanced info **************************/
	
	public void showAdvancedDrug(JTextArea textArea, JList list, String a, JPanel contentPane){
		String name = "\""+(runnerList.get(list.getSelectedIndex())+"\"");
		int id = 0;
		float cena = 0;
		textArea.setText("");
		textArea.append("ID\tLiek\tHolder\tForma Lieku\tCena\tStav\tVydaj\tLieciva Latka\n" +
				"-------------------------------------------------------------------" +
				"----------------------------------------------------------------------------" +
				"-------------------------------------\n");
		
		try {
			for(Drugs drugs : adr.showOneAdvancedDrug(name, a)){
				textArea.append(drugs.getId()+"\t"+drugs.getName() + "\t" + drugs.getHolder() +"\t"+drugs.getForm()+"\t"+drugs.getPrice()
						+"\t"+ drugs.getStav() + "\t"+ drugs.getVydaj() + "\t"+drugs.getLatka()+"\t"+"\n");
				
				id = drugs.getId();
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(contentPane, "Error","File error!", JOptionPane.ERROR_MESSAGE);
		}
		if(id != 0){
			textArea.append("\nPrehlad ceny lieku:\nod\tdo\tcena\n" +
					"-----------------------------------------------------------\n");
			try {
				for(ActualPrice price : pr.showAllPrice(id)){
					textArea.append(price.getFrom()+"\t"+price.getTo()+"\t"+price.getPrice()+"\n");
				}
			} catch (SQLException e) {
			e.printStackTrace();
			}
		
		
		try {
			textArea.append("\n\nPriemerna cena lieku: ");
			for(ActualPrice price : pr.showAVGPrice(id)){
					textArea.append("\n"+price.getPrice()+"");
			}
			textArea.append("\n\nMin. cena lieku: ");
			for(ActualPrice price : pr.showMinPrice(id)){
				textArea.append("\n"+price.getPrice());
				cena = price.getPrice();
		}
			for(ActualPrice price : pr.showMinDate(cena,id)){
				textArea.append("\n\nV case:\nod: "+price.getFrom() +" do: "+ price.getTo());
				
		}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		}
	}
	
	public void infoDoctor(JTextArea textArea){
		textArea.setText("ID\tplat\tkontakt\tmeno\tpredpisy\n" +
				"--------------------------------------------------------" +
				"---------------------------------------------------------------\n");
		try {
			for(Doctor doctor : doc.showDoctorInfo()){
				textArea.append(doctor.getId()+"\t"+doctor.getSalary()+"\t"+("0"+doctor.getContact())+"\t"+doctor.getName()+"\t"+doctor.getPocet()+"\n");	
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void clearAll(){
		runnerList.clear();
		runnerList2.clear();
		runnerList3.clear();
		runnerList4.clear();
		runnerList5.clear();
		runnerList6.clear();
		runnerList7.clear();
	}
	
	
}
