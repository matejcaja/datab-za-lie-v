package dbs2013.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.model.ActualPrice;


public class PriceManager extends DatabaseConnect {

	@Override
	protected ActualPrice processRow(ResultSet rs) throws SQLException {
		
		return new ActualPrice(rs.getInt("id"), rs.getString("from"), rs.getString("to"),rs.getFloat("price"), rs.getInt("drug_id"));
	}

	@SuppressWarnings("unchecked")
	public List<ActualPrice> showAllPrice(Integer id) throws SQLException{
		return selectQuery("select * from ActualPrice where drug_id = "+id);
	}
	
	@SuppressWarnings("unchecked")
	public List<ActualPrice> showAVGPrice(Integer id) throws SQLException{
		return selectQuery("select a.id, a.from, a.to, avg(a.price) as price, a.drug_id from ActualPrice a where drug_id = "+id);
	}
	
	@SuppressWarnings("unchecked")
	public List<ActualPrice> showMinPrice(Integer id) throws SQLException{
		return selectQuery("select a.id, a.from, a.to, min(a.price) as price, a.drug_id from ActualPrice a where drug_id = "+id);
	}
	
	@SuppressWarnings("unchecked")
	public List<ActualPrice> showMinDate(Float price, Integer id) throws SQLException{
		return selectQuery("select * from ActualPrice a where drug_id = "+id+" and CAST(price as decimal) = CAST("+price+"as decimal)");
	}
}
