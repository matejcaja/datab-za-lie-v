package dbs2013.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.model.Contents;


public class ContentsManager extends DatabaseConnect{
		
	protected Object processRow(ResultSet rs) throws SQLException {
		return(new Contents(rs.getInt("d.id"), rs.getString("name"), rs.getString("holder"), rs.getString("vydaj"),
				rs.getString("a.price"), rs.getInt("pocet")));
	}
	
	@SuppressWarnings("unchecked")
	public List<Contents> getAll() throws SQLException{
		return selectQuery("Select *, d.name, e.state from Contents c join Drugs d on c.drug_id = d.id join EffectiveChemical e on e.id = c.chemical_id");
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Contents> getAllInfo() throws SQLException{
		
		return selectQuery("Select d.id,d.name,d.holder,d.vydaj, a.price, count(e.name) as pocet from Contents c join Drugs d on c.drug_id = d.id join EffectiveChemical e on e.id = c.chemical_id " +
				"join ActualPrice a on a.drug_id = d.id where a.to is null and d.stav like 'able' group by d.id");
	}
	@SuppressWarnings("unchecked")
	public List<Contents> getAdvancedInfo(String data, String order, String able, String price, String count) throws SQLException{
		return selectQuery("Select d.id,d.name,d.holder,d.vydaj, a.price, count(e.name) as pocet from Contents c join Drugs d on c.drug_id = d.id join EffectiveChemical e on e.id = c.chemical_id " +
				"join ActualPrice a on a.drug_id = d.id where a.to is null and d.stav like" +able+" and d.holder like " +data+ " and price < "+price+"" +
						" group by d.id having pocet = "+count+" order by d.id " +order);
	}
	
	
}
