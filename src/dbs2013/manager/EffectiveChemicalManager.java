package dbs2013.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.model.EffectiveChemical;

public class EffectiveChemicalManager extends DatabaseConnect{

	@Override
	protected Object processRow(ResultSet rs) throws SQLException {
		
		return(new EffectiveChemical(rs.getInt("id"), rs.getString("name")));
	}
	
	@SuppressWarnings("unchecked")
	public List<EffectiveChemical> listOfEffectiveChemical() throws SQLException{
		return selectQuery("select e.id,e.name from Drugs d "+
				"join Contents c on c.drug_id = d.id "+ 
				"join EffectiveChemical e on e.id = c.chemical_id "+
				"group by e.id order by e.id");
	}

}
