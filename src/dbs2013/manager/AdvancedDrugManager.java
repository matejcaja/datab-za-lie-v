package dbs2013.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.model.Drugs;

public class AdvancedDrugManager extends DatabaseConnect{

	@Override
	protected Object processRow(ResultSet rs) throws SQLException {
		return new Drugs(rs.getString("name"), rs.getString("holder"), rs.getString("stav"), 
				rs.getString("vydaj"), rs.getString("type"), rs.getString("price"), rs.getString("e.name"), rs.getInt("d.id"));
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> showOneAdvancedDrug(String name, String holder) throws SQLException{
		return selectQuery("select *,s.type_id from Drugs d "+
				"join DrugsSpecification s on s.drug_id = d.id "+
				"join DrugsForm f on f.id = s.type_id "+
				"join ActualPrice a on a.drug_id = d.id " +
				"join Contents c on c.drug_id = d.id "+
				"join EffectiveChemical e on e.id = c.chemical_id " +
				"where d.name like "+ name+ " and d.holder like " +holder+" and a.to is null");
	}
		
}
