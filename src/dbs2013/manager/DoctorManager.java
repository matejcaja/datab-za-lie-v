package dbs2013.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.model.Doctor;

public class DoctorManager extends DatabaseConnect{

	@Override
	protected Doctor processRow(ResultSet rs) throws SQLException {
		
		return new Doctor(rs.getInt("id"), rs.getString("name"), rs.getFloat("salary"), rs.getInt("contact"), rs.getString("pocet"));
	}
	
	@SuppressWarnings("unchecked")
	public List<Doctor> showDoctorInfo() throws SQLException{
		return selectQuery("select *,count(p.drug_id) as pocet from Doctor d left join Prescript p on d.id = p.doctor_id group by d.id");
	}
	
	
}
