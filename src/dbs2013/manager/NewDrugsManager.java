package dbs2013.manager;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dbs2013.manager.DatabaseConnect;
import dbs2013.model.Drugs;


public class NewDrugsManager  extends DatabaseConnect{

	@Override
	protected Drugs processRow(ResultSet rs) throws SQLException {
		return(new Drugs(rs.getInt("id")));
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> listIdofNewDrug(String name) throws SQLException{
		return selectQuery("select d.id from Drugs d where d.name like "+name);
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> insertIntoDrugs(String name, String holder, String stav, String vydaj) throws SQLException{
		return updateQuery("insert into Drugs (`name`, `holder`, `stav`, `vydaj`) values " +
				"("+name+","+holder+","+stav+","+vydaj+")");
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> insertIntoPrice(Integer drug_id, String price) throws SQLException{
		return updateQuery("insert into ActualPrice (`from`,`price`,`drug_id`) values " +
				"(curdate(),"+price+","+drug_id+")");
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> insertIntoForm(Integer drug_id, Integer type_id) throws SQLException{
		return updateQuery("insert into DrugsSpecification (`type_id`,`drug_id`,`sell_start`) values " +
				"("+type_id+","+drug_id+",curdate())");
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Drugs> insertIntoContents(Integer drug_id, Integer chemical_id) throws SQLException{
		
		return updateQuery("insert into Contents (`drug_id`,`chemical_id`) values " +
				"("+drug_id+","+chemical_id+")");
		
	}

}
